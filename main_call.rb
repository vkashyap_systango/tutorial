require 'pry'
require_relative 'SalesTaxCalculator.rb'
module	Country
  IN = "INDIA"
  EG = "EGYPT"
  JP = "JAPAN"
end
module Files
  INPUT = 'product_list.csv'
  OUTPUT= 'product_list_with_salestax.csv'
end
  
  puts "Enter input file name: "
	input_file_name = $stdin.read
	puts "Enter output file name: "
	output_file_name = $stdin.read
	sales_tax_calculator = SalesTaxCalculator.new(input_file_name = Files::INPUT, output_file_name = Files::OUTPUT)
	sales_tax_calculator.process
