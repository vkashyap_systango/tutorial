require 'csv'
require 'pry'
require_relative 'FileChecker'

class OutputWriter
  def initialize(output)
    @output_file_name = output
    if(FileChecker.check_existance(@output_file_name) and FileChecker.check_write_permission(@output_file_name))
      puts "your file found and it is writable!!!"
    else
      raise "No such file #{@input_file_name}"
    end
  end

  def write_into_file
    CSV.open(@output_file_name, 'a+') do |row_calculated|
      RecordContainer.instance.get_product_array.each do |row|
        row_calculated << [row.get_id,row.get_name,row.get_price,row.get_country,row.get_sales_tax,row.get_final_price]
      end
    end
  end
end