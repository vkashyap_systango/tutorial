require_relative 'IndiaStrategy'
require_relative 'EgyptStrategy'
require_relative 'JapanStrategy'
require_relative 'Product'

class TaxStrategyFactory
	def get_strategy(product)
		if(product.get_country == Country::IN)
      return IndiaStrategy.new(product)
    elsif (product.get_country == Country::EG)
      return EgyptStrategy.new(product)
    elsif (product.get_country == Country::JP)
      return JapanStrategy.new(product)
    end
	end
end