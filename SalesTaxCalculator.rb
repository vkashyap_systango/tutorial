require_relative 'InputReader.rb'
require_relative 'OutputWriter.rb'
require_relative 'RecordContainer.rb'
require_relative 'RecordInitializer.rb'
require_relative 'ReocrdProcessor'


class SalesTaxCalculator

  def initialize (input, output)
    @input_file_name  = input
    @output_file_name = output
  end

  def process
    input_reader = InputReader.new(@input_file_name)
    record_initialize = RecordInitializer.new(input_reader)
    record_processor = ReocrdProcessor.new
    out_writer = OutputWriter.new(@output_file_name)

    record_initialize.populate_products
    record_processor.process_record
    out_writer.write_into_file

    puts "Your output file #{@output_file_name} is successfully generated!!!"
  end
end