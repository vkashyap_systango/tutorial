require_relative 'Product.rb'

class RecordInitializer
  def initialize(in_reader)
    @input_reader = in_reader
  end

  def populate_products 
    file = @input_reader.read_file
    file.each do |row|
      product_object = Product.new((get_id(row)),(get_name(row)),(get_price(row)),(get_country(row)))
      RecordContainer.instance.add(product_object)
    end
  end

  def get_id(row)
    return row[0].to_i rescue 0
  end
  def get_name(row)
    return row[1].to_s rescue ''
  end
  def get_price(row)
    return row[2].to_i rescue 0
  end
  def get_country(row)
    return row[3].to_s rescue ''
  end
  private :get_id, :get_name, :get_price, :get_country
end