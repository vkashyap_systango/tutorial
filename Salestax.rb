  require 'csv'
  require 'pry'
    IN="INDIA"
    EG="EGYPT"
    JP="JAPAN"
    INPUT_FILENAME  = 'product_list.csv'
    OUTPUT_FILENAME = 'product_list_with_salestax.csv'
    $product_row = Array.new
    $price
    
    $sales_tax=0.0
    $actual_price
    $i =  true
def create_output_file_with_first_row
  product_line = ["ID","Name","Price","Country","Sales Tax","Actual Price"]
  CSV.open(OUTPUT_FILENAME, 'w') do |row_calculated|
    row_calculated << product_line
  end
end

def compute_sales_tax_india
  if $price < 100
    $sales_tax = 0

  elsif ($price >= 100 and $price < 500)
    $sales_tax = ($price * 10)/100
    
    elsif ($price >= 500 and $price < 1000)
      $sales_tax = ($price * 15)/100
      elsif $price >= 1000
        $sales_tax = ($price * 20)/100
  end
end

def compute_sales_tax_egypt
  if $price > 1000
    $sales_tax = ($price * 30)/100
  else
    $sales_tax = 0
  end
end

def compute_sales_tax_japan
  if $price > 10000
    $sales_tax = ($price * 2)/100
  else
    $sales_tax = 0
  end
end

def calculate_actual_price
  $actual_price = $price + $sales_tax
end

def insert_row_to_output_file
  CSV.open('product_list_with_salestax.csv', 'a+') do |row_calculated|
    row_calculated << $product_row
  end
end

CSV.foreach(INPUT_FILENAME, converters: :numeric) do |row|
  $price = row[2]
  country = row[3]
  if $i==true
    $i=false
    create_output_file_with_first_row
    next
  end

  if country == IN
      compute_sales_tax_india      
  elsif country == EG
      compute_sales_tax_egypt
    elsif country == JP
      compute_sales_tax_japan
  end
  # $product_row = [id , name , $price , country , $sales_tax , $actual_price]
  #binding.pry
  $product_row = row
  insert_row_to_output_file  
end



   