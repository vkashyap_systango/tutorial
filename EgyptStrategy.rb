require_relative 'Product'
require_relative 'Strategy'
require_relative 'TaxStrategyFactory'

class EgyptStrategy < Strategy
  def initialize(product)
    @product = product
  end
  def calculate_tax()
    if @product.get_price > 1000
      sales_tax = (@product.get_price * 30)/100
    else
      sales_tax = 0
    end  
    return sales_tax
  end
end