require_relative 'TaxStrategyFactory.rb'
require_relative 'IndiaStrategy'
require_relative 'EgyptStrategy'
require_relative 'JapanStrategy'

class Product

  def initialize (id,name,price,country)
    @id = id
    @name =name
    @price =price
    @country = country
  end

  def process_product
    tax_calculation_factory = ::TaxStrategyFactory.new()
    tax_calculation_strategy = tax_calculation_factory.get_strategy(self)
    set_sales_tax(tax_calculation_strategy.calculate_tax())
    calculate_final_price

  end

  def set_sales_tax(sales_tax)
    @sales_tax = sales_tax
  end
  def get_id
    return @id
  end
  def get_name
    return @name
  end
  def get_price
    return @price
  end
  def get_country
    return @country
  end
  def get_sales_tax
    return @sales_tax
  end
  def get_final_price
    return @final_price
  end
  
  private
  def calculate_final_price
    @final_price = @price + @sales_tax
  end

 
  
end
