require 'csv'
require 'pry'
  IN="INDIA"
  EG="EGYPT"
  JP="JAPAN"
  INPUT_FILENAME  = 'product_list.csv'
  OUTPUT_FILENAME = 'product_list_with_salestax.csv'
  class Product
     @sales_tax=0, @final_price=0

    def initialize (id,name,price,country)
      @id = id
      @name =name
      @price =price
      @country = country
    end

    def calculate_sales_tax
      if @country == IN
        compute_sales_tax_india      
      elsif @country == EG
        compute_sales_tax_egypt
        elsif @country == JP
          compute_sales_tax_japan
      end
    end 

    def get_id
      return @id
    end
    def get_name
      return @name
    end
    def get_price
      return @price
    end
    def get_country
      return @country
    end
    def get_sales_tax
      return @sales_tax
    end
    def get_final_price
      return @final_price
    end
    def calculate_final_price
      @final_price = @price + @sales_tax
    end

    def compute_sales_tax_india
      if @price < 100
        @sales_tax = 0

      elsif (@price >= 100 and @price < 500)
        @sales_tax = (@price * 10)/100
        
        elsif (@price >= 500 and @price < 1000)
          @sales_tax = (@price * 15)/100
          elsif @price >= 1000
            @sales_tax = (@price * 20)/100
      end
    end

    def compute_sales_tax_egypt
      if @price > 1000
        @sales_tax = (@price * 30)/100
      else
        @sales_tax = 0
      end
    end

    def compute_sales_tax_japan
      if @price > 10000
        @sales_tax = (@price * 2)/100
      else
        @sales_tax = 0
      end
    end


  end



  class InputReader
    @input_file_name

    def initialize(input)
      @input_file_name  = input
    end

    def validate_file
      if check_existance
        puts "file found"
        if check_read_permission
          puts "file is readable"
          return true
        else
          puts  "you don't have permission to read file #{@input_file_name}!!!"
          exit 1
        end
      else
        puts "Your file #{@input_file_name} doesn't exist!!"
        exit 1
      end
    end

    def check_read_permission
      return File.readable?(@input_file_name)
    end

    def check_existance
      return File.file?(@input_file_name)
    end

    def for_each_row(row_number)
      @i=0
      CSV.foreach(@input_file_name, converters: :numeric) do |row|
        if @i == row_number
          return row
        end
        @i
      end

    end

    def read_file
      @row_counter=0
      @arr_row =Array.new
      CSV.foreach(@input_file_name, converters: :numeric) do |row|
        
        if(@row_counter>=1)
          @arr_row << row
          #puts row.inspect
        end
        @row_counter = @row_counter +1
      end

      return @arr_row
    end

  end

  class RecordInitializer
    def initialize(in_reader, rec_container)
      @input_reader = in_reader
      @record_container = rec_container
    end

    def product_initializer 
      file = @input_reader.read_file
      file.each do |row|
       # puts row.inspect
        product_object = Product.new((get_id(row)),(get_name(row)),(get_price(row)),(get_country(row)))
        @record_container.add(product_object)
      end

    end

    def get_id(row)
      return row[0]
    end
    def get_name(row)
      return row[1]
    end
    def get_price(row)
      return row[2]
    end
    def get_country(row)
      return row[3]
    end

  end

  class RecordContainer
    @product_array = Array.new
    def initialize
      @product_array ||= []
    end

    def add (product_obj)
      @product_array << product_obj
    end

    def for_each
      
    end

    def get_product_array()
      return @product_array
      
    end
  end

  class ReocrdProcessor
    def initialize (rec_container)
      @record_container = rec_container
    end
    def process_record

      @record_container.get_product_array.each do |product|
        product.calculate_sales_tax
        product.calculate_final_price
      end
    end
  end

  class OutputWriter
    @output_file_name

    def initialize(output,rec_container)
      @output_file_name = output
      @record_container =rec_container
    end

    def validate_file
      if check_existance
        puts "writeable file exists"
        if check_write_permission
          puts "you have permission to write"
          return true
        else
          puts  "you don't have permission to write in #{@output_file_name} file!!!"
          exit 1
        end
      else
        puts "Your file #{@output_file_name} doesn't exist!!"
        exit 1
      end
    end

    def check_write_permission
      return File.writable?(@output_file_name)
    end

    def check_existance
      return File.file?(@output_file_name)
    end

    def write_into_file
      product_line = ["ID","Name","Price","Country","Sales Tax","Actual Price"]
      CSV.open(OUTPUT_FILENAME, 'w') do |first_row|
        first_row << product_line
      end
        CSV.open(@output_file_name, 'a+') do |row_calculated|
        @record_container.get_product_array.each do |row|
          row_calculated << [row.get_id,row.get_name,row.get_price,row.get_country,row.get_sales_tax,row.get_final_price]

        end
      end
    end
  end
  class SalesTaxCalculator
   # @input_file_name, @output_file_name

    def initialize (input, output)
      @input_file_name  = input
      @output_file_name = output
    end

    def process
      input_reader = InputReader.new(@input_file_name)
      record_container = RecordContainer.new
      record_initialize = RecordInitializer.new(input_reader , record_container)
      record_processor = ReocrdProcessor.new(record_container)
      out_writer = OutputWriter.new(@output_file_name, record_container)

      input_reader.validate_file
      record_initialize.product_initializer
      record_processor.process_record
      out_writer.validate_file
      out_writer.write_into_file
      puts "Your output file #{@output_file_name} is successfully generated!!!"
    end
  end






#puts "Enter input file name: "
#input_file_name = $stdin.read
#puts "Enter output file name: "
#output_file_name = $stdin.read
input_file_name = INPUT_FILENAME
output_file_name = OUTPUT_FILENAME
sales_tax_calculator = SalesTaxCalculator.new(input_file_name, output_file_name)
sales_tax_calculator.process

