require_relative 'Product'
require_relative 'Strategy'
require_relative 'TaxStrategyFactory'

class IndiaStrategy < Strategy
  def initialize(product)
    @product = product
  end
  def calculate_tax()
   if @product.get_price < 100
       sales_tax = 0
    elsif (@product.get_price >= 100 and @product.get_price < 500)
        sales_tax = (@product.get_price * 10)/100
      elsif (@product.get_price >= 500 and @product.get_price < 1000)
        sales_tax = (@product.get_price * 15)/100
        elsif @product.get_price >= 1000
          sales_tax = (@product.get_price * 20)/100
    end
    return sales_tax  
  end
end