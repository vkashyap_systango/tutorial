require 'singleton'
class RecordContainer
  include Singleton
  def initialize
    @product_array ||= []
  end

  def add (product_obj)
    @product_array << product_obj
  end

  def get_product_array()
    return @product_array    
  end
end
